<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is the MAIN page</p>
<hr>
Your Login status: 
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"], ", Your ID is: ", $_SESSION["loginProfile"]["uID"],"<hr>";
	if($_SESSION["loginProfile"]["uRole"] == 9) {
		$result=getOrderListAdmin($_SESSION["loginProfile"]['uID']);
	}
	else{
		$result=getOrderList($_SESSION["loginProfile"]['uID']);
	}
?>
	<table width="350" border="1">
  <tr>
    <td>order ID</td>
    <td>Date</td>
    <td>Status</td>
	<td>Operate</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td>" . $rs['ordID'] . "</td>";
	echo "<td>{$rs['orderDate']}</td>";
	echo "<td>" , $rs['status'], "</td>";
	if($_SESSION["loginProfile"]["uRole"] == 9) {
		echo "<td><a href='EditStatus.php?ordID=" , $rs['ordID'] , "'>Edit</a>&nbsp&nbsp&nbsp<a href='ShowList.php?ordID=" , $rs['ordID'] , "'>查看</a></td>";
	}
	else if($_SESSION["loginProfile"]["uRole"] == 1){
		echo "<td><a href='ShowList.php?ordID=" , $rs['ordID'] , "'>查看</a></td>";
	}
	echo "</tr>";
}
?>

</table>
<a href="main.php">OK</a><hr>

</body>
</html>
